require (
	github.com/andygrunwald/go-jira v1.12.0
	github.com/cenkalti/backoff/v4 v4.0.0
	github.com/pkg/errors v0.9.1
	github.com/xanzy/go-gitlab v0.27.0
	gopkg.in/go-playground/webhooks.v5 v5.13.0
	gopkg.in/yaml.v3 v3.0.0-20191120175047-4206685974f2
)

module art

go 1.14
