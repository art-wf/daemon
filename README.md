# How to run

```bash
docker run --name art-daemon -p 80:80 -v $(pwd)/config.yml:/etc/art/config.yml registry.gitlab.com/art-wf/daemon:0.0.2
```

# How to configure

Create `config.yml` and mount it inside docker container with path `/etc/art/config.yml`

```yaml
retries: # retries if job can not be played
  interval: 3m # interval between tries
  attempts: 60 # max try attempts

mr_events: # merge request event to job name
  approved: ART/ready to test
  unapproved: ART/review

jira_transitions: # jira status name to job name
  Ready to test: ART/ready to test
  Review: ART/review
  Ready to release: ART/ready to release
  In progress: ART/in progress
  Done: ART/close release
```

# Container environment

* **ART_GITLAB_LOGIN** using for basic authorization and ignore bot events
* **ART_GITLAB_PASSWORD** using for basic authorization
* **ART_GITLAB_TOKEN** using for authorization by private token. token is priority method.
* **ART_JIRA_LOGIN** using for basic authorization and ignore bot events
* **ART_JIRA_PASSWORD** using for basic authorization
* **ART_JIRA_TOKEN** using for authorization. token is priority method.
* **ART_DAEMON_TOKEN** auth token for requests to daemon
