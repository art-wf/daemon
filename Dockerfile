FROM golang:alpine
WORKDIR /go/src/art
COPY . .
RUN go install -v ./... && ls -la /go/bin

FROM alpine:latest
COPY --from=0 /go/bin /usr/bin
WORKDIR /etc/art

ENTRYPOINT ["art"]