package main

import (
	"fmt"
	"github.com/andygrunwald/go-jira"
	"github.com/pkg/errors"
	gitlabClient "github.com/xanzy/go-gitlab"
	gitlabHooks "gopkg.in/go-playground/webhooks.v5/gitlab"
	"regexp"
	"strconv"
	"strings"
)

type TransitionEvent struct {

	Transition struct {
		FromStatus string `json:"from_status"`
		ToStatus   string `json:"to_status"`
	} `json:"transition"`

	Issue jira.Issue `json:"issue"`

	User struct {
		Name string `json:"name"`
	} `json:"user"`
}

func getIssueMRs(jiraCli *jira.Client, gitlabCli *gitlabClient.Client, issueKey string) (map[int]*gitlabClient.MergeRequest, error) {

	issueMRs := make(map[int]*gitlabClient.MergeRequest)

	links, _, err := jiraCli.Issue.GetRemoteLinks(issueKey)
	if err != nil {
		return nil, errors.Wrap(err, "cannot get issue links")
	}

	gitlabUrl := gitlabCli.BaseURL()
	mrLinkFormat := regexp.MustCompile(
		fmt.Sprintf(`^%s://%s/(.+)/merge_requests/(\d+)$`, gitlabUrl.Scheme, gitlabUrl.Host),
	)
	for _, link := range *links {
		if mrLinkFormat.MatchString(link.Object.URL) {
			mrData := mrLinkFormat.FindStringSubmatch(link.Object.URL)

			projectID := strings.TrimSuffix(mrData[1], "/-")

			mrID, err := strconv.Atoi(mrData[2])
			if err != nil {
				return nil, errors.Wrap(err, "cannot convert to int")
			}

			mr, _, err := gitlabCli.MergeRequests.GetMergeRequest(projectID, mrID, &gitlabClient.GetMergeRequestsOptions{})
			if err != nil {
				return nil, errors.Wrap(err, "cannot get merge request")
			}

			for _, label := range mr.Labels {
				if label == "ART" && mr.State == "opened" {
					issueMRs[mr.IID] = mr
				}
			}
		}
	}

	return issueMRs, nil
}

func getRelatedMRs(jiraCli *jira.Client, gitlabCli *gitlabClient.Client, mr *gitlabHooks.MergeRequestEventPayload) (map[int]*gitlabClient.MergeRequest, error) {
	issueKey, err := parseIssueKey(mr.ObjectAttributes.SourceBranch)
	if err != nil {
		return nil, errors.Wrap(err, "cannot parse issue key")
	}

	issueMRs, err := getIssueMRs(jiraCli, gitlabCli, issueKey)
	if err != nil {
		return nil, errors.Wrap(err, "cannot get issue MRs")
	}

	delete(issueMRs, int(mr.ObjectAttributes.IID))
	return issueMRs, nil
}

func parseIssueKey(str string) (string, error) {
	issueKeyMatcher := regexp.MustCompile(`\w+-\d+`)
	if !issueKeyMatcher.MatchString(str) {
		return "", errors.Errorf("%s doesn't contain issue key", str)
	}
	return issueKeyMatcher.FindStringSubmatch(str)[0], nil
}