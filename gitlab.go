package main

import (
	"github.com/pkg/errors"
	gitlabClient "github.com/xanzy/go-gitlab"
)

func checkMRApproval(gitlabCli *gitlabClient.Client, mr *gitlabClient.MergeRequest) error {
	approvals, _, err := gitlabCli.MergeRequests.GetMergeRequestApprovals(mr.ProjectID, mr.IID)
	if err != nil {
		return errors.Wrap(err, "cannot get MR approvals")
	}

	if len(approvals.ApprovedBy) == 0 || approvals.ApprovalsLeft > 0 {
		return errors.Errorf("MR wait for approve: %s", mr.WebURL)
	}

	return nil
}