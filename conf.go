package main

import "time"

type Config struct {

	Retries struct {
		Interval time.Duration `yaml:"interval"`
		Attempts uint64        `yaml:"attempts"`
	} `yaml:"retries"`

	MREvents map[string]string `yaml:"mr_events"`

	JiraTransitions map[string]string `yaml:"jira_transitions"`
}